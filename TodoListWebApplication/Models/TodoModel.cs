﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Todo_Domain_Entities;

namespace TodoListWebApplication.Models
{
    public class TodoModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Todo's Name")]
        [MaxLength(40)]
        public string Name { get; set; }

        [Display(Name= "Todo Description")]
        [MaxLength(255)]
        public string Description { get; set; }

        [Display(Name = "Toso's Status")]
        public TodoStatus Status { get; set; }

        [Display(Name = "Time Of Creation")]
        public DateTime CreationTime { get; set; } = DateTime.Now;

        [Display(Name = "Due Time")]
        [DataType(DataType.DateTime)]
        public DateTime? DueTime { get; set; }
        public int TodoListID { get; set; }

        [JsonIgnore]
        public TodoListModel TodoList { get; set; }

        public string CleanDueTime()
        {
            return DueTime.HasValue ? DueTime.Value.ToString("dd/MM/yy HH:mm") : "No Due Date Yet";
        }
        public string CleanEnum()
        {
            return Status switch
            {
                TodoStatus.Completed => "Completed",
                TodoStatus.InProgress => "In Progress",
                TodoStatus.NotStarted => "Not Started",
                _ => "No Enum Selected",
            };
        }
    }
}
