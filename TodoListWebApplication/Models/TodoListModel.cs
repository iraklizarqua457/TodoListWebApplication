﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TodoListWebApplication.Models
{
    public class TodoListModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "List Name is required")]
        [Display(Name = "Todo List Name")]
        [MaxLength(50)]
        public string ListName { get; set; }

        [Required, Display(Name = "IsHidden")]
        public bool IsHidden { get; set; } = false;

        [Display(Name = "Time Of Creation")]
        public DateTime CreationTime { get; set; } = DateTime.Now;

        [JsonIgnore]
        public ICollection<TodoModel> Todos { get; set; }
        public string CleanBool()
        {
            return IsHidden ? "Visible" : "Not Visible";
        }
    }
}
