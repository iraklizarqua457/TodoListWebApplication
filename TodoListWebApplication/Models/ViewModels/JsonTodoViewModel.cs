﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoListWebApplication.Models
{
    public class JsonTodoViewModel
    {
        public string JsonTodoList { get; set; }
        public IEnumerable<string> JsonTodos { get; set; }
    }
}
