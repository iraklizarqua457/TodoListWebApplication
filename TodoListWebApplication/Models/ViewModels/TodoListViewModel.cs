﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Todo_Domain_Entities;

namespace TodoListWebApplication.Models
{
    public class TodoListViewModel
    {
        public TodoListModel TodoList { get; set; }
        public IEnumerable<TodoModel> Todos { get; set; }
    }
}
