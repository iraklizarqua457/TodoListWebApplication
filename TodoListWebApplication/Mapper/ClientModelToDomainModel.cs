﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo_Domain_Entities;
using TodoListWebApplication.Models;

namespace TodoListWebApplication.Mapper
{
    public static class ClientModelToDomainModel
    {
        public static Todo TodoClientToDomainModel(this TodoModel todo)
        {
            return new Todo()
            {
                ID = todo.ID,
                Name = todo.Name,
                Description = todo.Description,
                CreationTime = todo.CreationTime,
                DueTime = todo.DueTime.HasValue ? todo.DueTime.Value : todo.DueTime,
                Status = todo.Status,
                TodoListID = todo.TodoListID
            };
        }
        public static TodoList TodoListClientToDomainModel(this TodoListModel todoList)
        {
            return new TodoList()
            {
                ID = todoList.ID,
                ListName = todoList.ListName,
                IsHidden = todoList.IsHidden,
                CreationTime = todoList.CreationTime,
                Todos = todoList.Todos != null ?
                (from toDo in todoList.Todos select toDo.TodoClientToDomainModel()).ToArray(): null,
            };
        }
        public static IEnumerable<TodoList> ListOfTodoListsClientToDomainModel(this IEnumerable<TodoListModel> enumer)
        {
            return enumer.Select(x => x.TodoListClientToDomainModel());
        }
        public static IEnumerable<Todo> ListOfTodoClientToDomainModel(this IEnumerable<TodoModel> enumer)
        {
            return enumer.Select(x => x.TodoClientToDomainModel());
        }
    }
}
