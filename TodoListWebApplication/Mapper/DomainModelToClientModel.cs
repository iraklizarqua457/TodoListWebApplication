﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo_Domain_Entities;
using TodoListWebApplication.Models;

namespace TodoListWebApplication.Mapper
{
    public static class DomainModelToClientModel
    {
        public static TodoModel TodoDomainClientModel(this Todo todo)
        {
            return new TodoModel()
            {
                ID = todo.ID,
                Name = todo.Name,
                Description = todo.Description,
                CreationTime = todo.CreationTime,
                DueTime = todo.DueTime,
                Status = todo.Status,
                TodoListID = todo.TodoListID
            };
        }
        public static TodoListModel TodoListDomainToClientModel(this TodoList todoList)
        {
            return new TodoListModel()
            {
                ID = todoList.ID,
                ListName = todoList.ListName,
                IsHidden = todoList.IsHidden,
                CreationTime = todoList.CreationTime,
                Todos = todoList.Todos != null ?
                (from toDo in todoList.Todos select toDo.TodoDomainClientModel()).ToArray() : null,

            };
        }
        public static IEnumerable<TodoListModel> ListOfTodoListsDomainToClientModel(this IEnumerable<TodoList> enumer)
        {
            return enumer.Select(x => x.TodoListDomainToClientModel());
        }
        public static IEnumerable<TodoModel> ListOfTodosDomainToClientModel(this IEnumerable<Todo> enumer)
        {
            return enumer.Select(x => x.TodoDomainClientModel());
        }
    }
}
