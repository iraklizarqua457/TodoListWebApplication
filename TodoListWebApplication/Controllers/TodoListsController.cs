﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TodoListWebApplication;
using Todo_Domain_Entities;
using Todo_Domain_Entities.Interfaces;
using TodoListWebApplication.Models;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Text.Json;
using TodoListWebApplication.Mapper;

namespace TodoListWebApplication.Controllers
{
    public class TodoListsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public TodoListsController(IUnitOfWork unitofwork)
        {
            _unitOfWork = unitofwork;
        }

        // GET: TodoListController
        public async Task<ViewResult> Index()
        {
            var todoLists = await _unitOfWork.TodoLists.GetAllAsync();
            _unitOfWork.Complete();

            return View("Index", todoLists.Where(x => !x.IsHidden).ListOfTodoListsDomainToClientModel());
        }

        // GET: TodoListController
        public async Task<ViewResult> HiddenLists()
        {
            var todoLists = await _unitOfWork.TodoLists.GetAllAsync();
            _unitOfWork.Complete();

            return View("Index", todoLists.Where(x => x.IsHidden).ListOfTodoListsDomainToClientModel());
        }
        // GET: TodoLists/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var todoList = await _unitOfWork.TodoLists.GetTodoListsWithTodosAsync(id);

            return View(new TodoListViewModel()
            {
                TodoList = todoList.TodoListDomainToClientModel(),
                Todos = todoList.Todos.ListOfTodosDomainToClientModel(),
            });
        }

        // GET: TodoLists/Create
        public ViewResult Create()
        {
            return View();
        }

        // POST: TodoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToActionResult> Create(TodoListModel todoList)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(Error));
            }

            await _unitOfWork.TodoLists.Add(todoList.TodoListClientToDomainModel());
            _unitOfWork.Complete();

            return RedirectToAction(nameof(Index));
        }

        // GET: TodoLists/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var todoList = await _unitOfWork.TodoLists.GetByIDAsync(id);

            return View(todoList.TodoListDomainToClientModel());
        }

        // POST: TodoLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  RedirectToActionResult Edit(int id,  TodoListModel todoList)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(Error), new { statusCode = StatusCodes.Status406NotAcceptable });
            }

            _unitOfWork.TodoLists.Update(todoList.TodoListClientToDomainModel());
            _unitOfWork.Complete();

            return RedirectToAction(nameof(Index));
        }

        // POST: TodoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var todoList = await _unitOfWork.TodoLists.GetByIDAsync(id);
            var todos = await _unitOfWork.Todo.GetTodosForTodoListWithID(id);
            _unitOfWork.Todo.RemoveRange(todos);
            await _unitOfWork.TodoLists.Remove(todoList);
            _unitOfWork.Complete();

            return RedirectToAction(nameof(Index));

        }
        [HttpGet]
        public async Task<IActionResult> Copy(int id)
        {
            var todoList = await _unitOfWork.TodoLists.GetTodoListsWithTodosAsync(id);

            var jsonTodoList = JsonSerializer.Serialize(todoList.TodoListDomainToClientModel());
            var jsonTodoEntries = from todo in todoList.Todos.ListOfTodosDomainToClientModel()
                                  select JsonSerializer.Serialize(todo);

            return View(nameof(Copy), new JsonTodoViewModel { JsonTodoList = jsonTodoList, JsonTodos = jsonTodoEntries });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int statusCode)
        {
            return View(new ErrorViewModel { StatusCode = statusCode, RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
