﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Todo_Domain_Entities;
using Todo_Domain_Entities.Interfaces;
using TodoListWebApplication.Mapper;
using TodoListWebApplication.Models;

namespace TodoListWebApplication.Controllers
{
    public class TodoController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public TodoController(IUnitOfWork unitofWork)
        {
            _unitOfWork = unitofWork;
        }
        // GET: TodoController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var todoEntry = await _unitOfWork.Todo.GetByIDAsync(id);
            return View(todoEntry.TodoDomainClientModel());
        }

        // GET: TodoController/Create/5
        public ActionResult Create(int listID)
        {
            ViewBag.ListID = listID;
            return View();

        }

        // POST: TodoController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToActionResult> Create(TodoModel todo)
        {
            try
            {
                await _unitOfWork.Todo.Add(todo.TodoClientToDomainModel());
                _unitOfWork.Complete();

                return RedirectToAction("Details", "TodoLists", new { id = todo.TodoListID });
            }
            catch
            {
                return RedirectToAction(nameof(Error), new { statusCode = StatusCodes.Status404NotFound });
            }
        }

        // GET: TodoController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var todoEntry = await _unitOfWork.Todo.GetByIDAsync(id);

            return View(todoEntry.TodoDomainClientModel());
        }

        // POST: TodoController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectToActionResult Edit(int id, TodoModel todo)
        {
            _unitOfWork.Todo.Update(todo.TodoClientToDomainModel());
            _unitOfWork.Complete();

            return RedirectToAction("Details", "TodoLists", new { id = todo.TodoListID });
        }

        // POST: TodoController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToActionResult> Delete(int id)
        {
            var todoEntry = await _unitOfWork.Todo.GetByIDAsync(id);

            await _unitOfWork.Todo.Remove(todoEntry);

            _unitOfWork.Complete();

            return RedirectToAction("Details", "TodoLists", new { id = todoEntry.TodoListID });

        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int statusCode)
        {
            return View(new ErrorViewModel { StatusCode = statusCode, RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
