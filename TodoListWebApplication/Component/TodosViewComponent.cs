﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo_Domain_Entities.Interfaces;
using TodoListWebApplication.Mapper;

namespace TodoListWebApplication.Component
{
    public class TodosViewComponent: ViewComponent
    {
        private readonly IUnitOfWork _unitOfWork;

        public TodosViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var todos = await _unitOfWork.Todo.GetTodosForTodoListWithID(id);
            var res = todos.ListOfTodosDomainToClientModel();
            if (!res.Any())
            {
                return View(res);
            }
            return View(res);
        }
    }
}
