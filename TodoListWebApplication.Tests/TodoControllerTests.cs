using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using Todo_Domain_Entities;
using Todo_Domain_Entities.Interfaces;
using Todo_Domain_Entities.Repository;
using TodoListWebApplication.Controllers;
using TodoListWebApplication.Models;

namespace TodoListWebApplication.Tests
{
    public class TodoControllerTests
    {
        private Mock<IUnitOfWork> mockRepo;
        private TodoController contr;

        [SetUp]
        public void Setup()
        {
            mockRepo = new Mock<IUnitOfWork>();
            contr = new TodoController(mockRepo.Object);
        }
        [Test]
        [TestCase(1)]
        public void DetailsValidID(int id)
        {
            mockRepo.Setup(repo => repo.Todo.GetByIDAsync(It.IsAny<int>()).Result)
                .Returns(GetTestTodos()[0]);

            var res = contr.Details(id).Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.Todo.GetByIDAsync(It.IsAny<int>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void CreateGETValidID(int todoListID)
        {
            var res = contr.Create(todoListID);

            Assert.That(res, Is.TypeOf<ViewResult>());
        }
        [Test]
        public void CreatePOSTValidID()
        {
            mockRepo.Setup(repo => repo.Todo.Add(It.IsAny<Todo>()));

            var res = contr.Create(GetTestTodoModels()[0]).Result;

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.Todo.Add(It.IsAny<Todo>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void EditGETValidID(int id)
        {
            mockRepo.Setup(repo => repo.Todo.GetByIDAsync(It.IsAny<int>()).Result)
                .Returns(GetTestTodos()[0]);

            var res = contr.Edit(id).Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.Todo.GetByIDAsync(It.IsAny<int>()), Times.Exactly(1));

        }
        [Test]
        public void EditPOSTValidID()
        {
            mockRepo.Setup(repo => repo.Todo.Update(It.IsAny<Todo>()));

            var res = contr.Edit(1, GetTestTodoModels()[0]);

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.Todo.Update(It.IsAny<Todo>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void DeleteValidID(int id)
        {
            mockRepo.Setup(repo => repo.Todo.GetByIDAsync(It.IsAny<int>()).Result)
                .Returns(GetTestTodos()[0]);
            mockRepo.Setup(repo => repo.Todo.Remove(It.IsAny<Todo>()));

            var res = contr.Delete(id).Result;

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.Todo.GetByIDAsync(It.IsAny<int>()), Times.Exactly(1));
            mockRepo.Verify(x => x.Todo.Remove(It.IsAny<Todo>()), Times.Exactly(1));
        }
        private List<Todo> GetTestTodos()
        {
            return new List<Todo>()
            {
                new Todo()
                {
                    ID = 1,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.NotStarted,
                },
                new Todo()
                {
                    ID = 2,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.Completed,
                },
                new Todo()
                {
                    ID = 3,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.InProgress,
                },
            };
        }
        private List<TodoModel> GetTestTodoModels()
        {
            return new List<TodoModel>()
            {
                new TodoModel()
                {
                    ID = 1,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.Completed,
                },
                new TodoModel()
                {
                    ID = 2,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.InProgress,
                },
                new TodoModel()
                {
                    ID = 3,
                    Name = "Some name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.NotStarted,
                },
            };
        }
    }
}