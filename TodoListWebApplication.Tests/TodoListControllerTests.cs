﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using Todo_Domain_Entities;
using Todo_Domain_Entities.Interfaces;
using Todo_Domain_Entities.Repository;
using TodoListWebApplication.Controllers;
using TodoListWebApplication.Models;
namespace TodoListWebApplication.Tests
{
    public class TodoListsControllerTests
    {
        private Mock<IUnitOfWork> mockRepo;
        private TodoListsController contr;

        [SetUp]
        public void Setup()
        {
            mockRepo = new Mock<IUnitOfWork>();
            contr = new TodoListsController(mockRepo.Object);
        }
        [Test]
        public void IndexReturnsTodoLists()
        {
            mockRepo.Setup(repo => repo.TodoLists.GetAllAsync().Result)
                .Returns(GetTestTodoLists());

            var res = contr.Index().Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.TodoLists.GetAllAsync(), Times.Exactly(1));
        }
        [Test]
        public void IndexReturnsHiddenTodoLists()
        {
            mockRepo.Setup(repo => repo.TodoLists.GetAllAsync().Result)
                .Returns(GetTestTodoLists());

            var res = contr.HiddenLists().Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.TodoLists.GetAllAsync(), Times.Exactly(1));
        }
        [Test]
        public void CreateGETReturnsViewResult()
        {
            var res = contr.Create();

            Assert.That(res, Is.TypeOf<ViewResult>());
        }
        [Test]
        public void CreatePostReturnsRedirectToActionResult()
        {
            mockRepo.Setup(repo => repo.TodoLists.Add(It.IsAny<TodoList>()));

            var res = contr.Create(GetTestTodoListModels()[0]).Result;

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.TodoLists.Add(It.IsAny<TodoList>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void EditGETValidID(int id)
        {
            mockRepo.Setup(repo => repo.TodoLists.GetByIDAsync(It.IsAny<int>()).Result)
                .Returns(GetTestTodoLists()[0]);

            var res = contr.Edit(id).Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.TodoLists.GetByIDAsync(It.IsAny<int>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void EditPostValidID(int id)
        {
            mockRepo.Setup(repo => repo.TodoLists.Update(GetTestTodoLists()[0]));

            var res = contr.Edit(id, GetTestTodoListModels()[0]);

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.TodoLists.Update(It.IsAny<TodoList>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void DeleteValidID(int id)
        {
            mockRepo.Setup(repo => repo.TodoLists.GetByIDAsync(It.IsAny<int>()).Result)
                .Returns(GetTestTodoLists()[0]);
            mockRepo.Setup(repo => repo.Todo.GetTodosForTodoListWithID(It.IsAny<int>()).Result)
                .Returns(GetTestTodos());
            mockRepo.Setup(repo => repo.TodoLists.Remove(It.IsAny<TodoList>()));
            mockRepo.Setup(repo => repo.Todo.RemoveRange(It.IsAny<IEnumerable<Todo>>()));

            var res = contr.Delete(id).Result;

            Assert.That(res, Is.TypeOf<RedirectToActionResult>());
            mockRepo.Verify(x => x.TodoLists.GetByIDAsync(It.IsAny<int>()), Times.Exactly(1));
            mockRepo.Verify(x => x.Todo.GetTodosForTodoListWithID(It.IsAny<int>()), Times.Exactly(1));
            mockRepo.Verify(x => x.TodoLists.Remove(It.IsAny<TodoList>()), Times.Exactly(1));
            mockRepo.Verify(x => x.Todo.RemoveRange(It.IsAny<IEnumerable<Todo>>()), Times.Exactly(1));
        }
        [Test]
        [TestCase(1)]
        public void CopyPostValidID(int id)
        {
            var todoList = GetTestTodoLists()[0];

            todoList.Todos = GetTestTodos();

            mockRepo.Setup(repo => repo.TodoLists.GetTodoListsWithTodosAsync(It.IsAny<int>()).Result)
                .Returns(todoList);

            var res = contr.Copy(id).Result;

            Assert.That(res, Is.TypeOf<ViewResult>());
            mockRepo.Verify(x => x.TodoLists.GetTodoListsWithTodosAsync(It.IsAny<int>()), Times.Exactly(1));
        }
        private List<TodoList> GetTestTodoLists()
        {
            return new List<TodoList>()
            {
                new TodoList()
                {
                    ID = 1,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = false,
                },
                new TodoList()
                {
                    ID = 2,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = true
                },
                new TodoList()
                {
                    ID = 3,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = false,
                }
            };
        }

        private List<TodoListModel> GetTestTodoListModels()
        {
            return new List<TodoListModel>()
            {
                new TodoListModel()
                {
                    ID = 1,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = false,
                },
                new TodoListModel()
                {
                    ID = 2,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = true,
                },
                new TodoListModel()
                {
                    ID = 3,
                    ListName = "Some Name",
                    CreationTime = System.DateTime.Today,
                    IsHidden = false,
                }
            };
        }

        private List<Todo> GetTestTodos()
        {
            return new List<Todo>()
            {
                new Todo()
                {
                    ID = 1,
                    Name = "Some Name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.InProgress,
                },
                new Todo()
                {
                    ID = 2,
                    Name = "Some Name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.NotStarted,
                },
                new Todo()
                {
                    ID = 3,
                    Name = "Some Name",
                    Description = "Some Description",
                    CreationTime = System.DateTime.Today,
                    Status = TodoStatus.Completed,
                },
            };
        }
    }
}
