﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Todo_Domain_Entities
{
    public class TodoList
    {
        public int ID { get; set; }
        public string ListName { get; set; }
        public bool IsHidden { get; set; }
        public string Description { get; set; }
        public DateTime CreationTime { get; set; }
        public ICollection<Todo> Todos { get; set; }
        public override string ToString()
        {
            return $"Id: {ID,-5}\nTitle: {ListName,-15}\nIsVisible: {IsHidden}";
        }
    }
}
