﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo_Domain_Entities.Interfaces;

namespace Todo_Domain_Entities.Repository
{
    public class TodoRepository : GenericRepo<Todo>, ITodoRepository
    {
        public TodoRepository(ApplicationDbContext context) : base(context) { }
        public ApplicationDbContext TodoListdbContext => _context as ApplicationDbContext;

        public async Task<List<Todo>> GetTodosForTodoListWithID(int id)
        {
            return await TodoListdbContext.Todos.Where(x => x.TodoListID == id).ToListAsync();
        }
    }
}
