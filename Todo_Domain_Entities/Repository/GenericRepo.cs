﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Todo_Domain_Entities.Interfaces;

namespace Todo_Domain_Entities.Repository
{
    public class GenericRepo<T> : IGenericRepository<T> where T : class
    {
        protected readonly DbContext _context;

        public GenericRepo(DbContext dbContext)
        {
            _context = dbContext;
        }

        public Task Add(T entry)
        {
            var res = _context.Set<T>().Add(entry);

            return Task.FromResult(res);
        }

        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().Where(expression).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIDAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public Task Remove(T entry)
        {
            var res = _context.Set<T>().Remove(entry);
            return Task.FromResult(res);
        }

        public void RemoveRange(IEnumerable<T> entries)
        {
            _context.Set<T>().RemoveRange(entries);
        }
        public Task Update(T entry)
        {
            var res = _context.Set<T>().Update(entry);

            return Task.FromResult(res);
        }
    }
}
