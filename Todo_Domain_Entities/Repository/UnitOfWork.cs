﻿using System;
using System.Collections.Generic;
using System.Text;
using Todo_Domain_Entities.Interfaces;

namespace Todo_Domain_Entities.Repository
{
#pragma warning disable S3881 // "IDisposable" should be implemented correctly
    public class UnitOfWork : IUnitOfWork
#pragma warning restore S3881 // "IDisposable" should be implemented correctly
    {
        private readonly ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            TodoLists = new TodoListRepository(_context);
            Todo = new TodoRepository(_context);
        }
        public ITodoListRepository TodoLists { get; private set; }

        public ITodoRepository Todo { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
