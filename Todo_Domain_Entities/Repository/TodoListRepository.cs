﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Todo_Domain_Entities.Interfaces;

namespace Todo_Domain_Entities.Repository
{
    public class TodoListRepository : GenericRepo<TodoList>, ITodoListRepository
    {
        public TodoListRepository(ApplicationDbContext context) : base(context) { }
        public ApplicationDbContext TodoListdbContext => _context as ApplicationDbContext;
        public async Task<TodoList> GetTodoListsWithTodosAsync(int id)
        {
            return await TodoListdbContext.TodoLists.Include(x => x.Todos).FirstOrDefaultAsync(x => x.ID == id);
        }
    }
}
