﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Todo_Domain_Entities.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        Task<T> GetByIDAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> expression);
        Task Add(T entry);
        Task Update(T entry);
        void RemoveRange(IEnumerable<T> entries);
        Task Remove(T entry);
    }
}
