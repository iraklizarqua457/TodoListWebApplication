﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Todo_Domain_Entities.Interfaces
{
   public interface ITodoListRepository : IGenericRepository<TodoList>
    {
        Task<TodoList> GetTodoListsWithTodosAsync(int id);
    }
}
