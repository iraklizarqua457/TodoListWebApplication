﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Todo_Domain_Entities.Interfaces
{
   public interface ITodoRepository : IGenericRepository<Todo>
    {
        Task<List<Todo>> GetTodosForTodoListWithID(int id);
    }
}
