﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo_Domain_Entities.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        public ITodoListRepository TodoLists { get; }
        public ITodoRepository Todo { get; }
        public int Complete();
    }
}
