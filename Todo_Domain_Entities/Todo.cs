﻿using System;
using System.ComponentModel.DataAnnotations;
using Todo_Domain_Entities;

namespace Todo_Domain_Entities
{
    public class Todo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TodoStatus Status { get; set; }
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
        public DateTime? DueTime { get; set; }
        public int TodoListID { get; set; }
        public TodoList TodoList { get; set; }
    }
}
